# MAPS #

__(M)RNA (A)SSEMBLY for (P)ROTEOGENOMIC(S)__, MAPS, is a Java command line tool for transcript assembly. MAPS was designed to assemble open 
reading frames (ORFs) which can be used as input for downstream proteomics searching as part of a proteogenomics pipeline. Unlike general purpose transcript 
assemblers, MAPS optimizes for both read support AND transcriptome diversity, which allows it to detect rare open reading frames. This is important for the 
detection of rare on non-canonical transcripts and open reading frames which would otherwise be missed. 

## Usage ##

MAPS v2.0 uses aligned reads from unsorted SAMs or BAMs to assemble a transcriptome while optimizing for tryptic peptide diversity.
Has options for controlling library diversity, using the read sequence to account for mutations, and basic coverage/FPKM/TPM calculation.
Developed by Max Shokhirev at the Salk Institute IGC Core (C) 2015-18.

__Usage: MAPS [opts] alignment.sam/bam [alignment2.sam/bam] ... [alignmentN.sam/bam]__


All exon-exon junctions, exons, SNPs, args, 3frame translated fasta, nucleotide fasta, and annotations are printed to prefix. junc exon SNPs args pep nuc and gtf files, respectively.
The sequence file used during mapping (genomic sequence) is required for ORF-level analyses, extension of transcripts up/down stream, consensus sequence construction, and low-memory mode.

### Options ###

* seq	A fasta file containing the genomic sequences of the chromosomes that the reads were mapped to. Keep chr names consistent. (Default=null)
* o	Output prefix used for outputing transcript files (Default=01-Feb-2018;17-58)
* p	The number of cores to use for read agglomeration. Run with -p 0 to turn on low-memory mode (high disk usage and slower). (Default=4)
* library	Uses library design to deconvolute strand identity of reads (un,f,r,fr,rf,ff,rr) (Default=fr)
* diversity	Value between 0 and 1 used to control the diversity of the assembled transcriptome (increase to increase transcriptome diversity/size). Use 1 to cycle through 0 to 1 in steps of 0.1 (Default=0.5)
* consensus	Uses a read voting scheme that adjusts for depth to account for mutations. Generates SNP file in pgSNP format. Adds notation to ORFs. (Default=false)
* v	Verbose. Set if you would like to see additional messages/stats during the run. (Default=false)
* fmax	ORFs will be generated with lengths <= fmax peptides (Default=1000000)
* fmin	ORFs will be generated with lengths => fmin peptides (Default=10)
* minMAPQ	Reads with MAPQ less than this will not be used! (Default=0)
* keepTempFiles	If running in low-memory mode (by setting -p 0), set to true to keep the temporary chr-separated fasta and sam files. (Default=false)
* printExons	Outputs assembled exons as a gff file. (Default=true)
* printJunctions	Outputs junctions as a gff file. (Default=true)
* mult	Coverage depth multiplier. Leave at 0 to automatically adjust by totalReads/10^6. Increase if skipping chromosomes. (Default=0)
* ext	The amount each read will be extended. Set to override the default of 300(1.0-stringency^2) (Default=null)
* noTranscriptExtension	MAPS will automatically extend transcripts until a stop codon is found in all frames and both directions. (Default=false)


## Quick Start ##

### Compiling and running MAPS ###

1. Download all java files to a directory of choice or clone the repository
2. Compile all java files using javac *.java
3. Run by typing java MAPS [options] alignment.sam/bam 

### Running MAPS from JAR ###

1. Download the latest jar file from this repository [MAPS.jar](https://bitbucket.org/shokhirev/maps/downloads/MAPS.jar)
2. Run by typing java -jar [MAPS.jar](https://bitbucket.org/shokhirev/maps/downloads/MAPS.jar). [options] alignment.bam 

### Run basic assembly ###

1. To try MAPS with a small portion of reads from hg19 chr1 RNA-Seq data, please download [chr1_first5m.bam](https://bitbucket.org/shokhirev/maps/downloads/chr1_first5m.bam).
2. Run MAPS or [MAPS.jar](https://bitbucket.org/shokhirev/maps/downloads/MAPS.jar) with chr1_first5m.bam as the input:

e.g. java -jar MAPS.jar -library rf -mult 178 chr1_first5m.bam

Note that the -library option is needed for correct strand orientation (depends on library type) and -mult is needed because we are only assembling a small proportion of reads which come from about 178 million fragments.

### Run assembly with consensus sequence calling, ORF extends, and SNP detection ###

1. To try MAPS with a small portion of reads from hg19 chr1 RNA-Seq data, please download [chr1_first5m.bam](https://bitbucket.org/shokhirev/maps/downloads/chr1_first5m.bam).
2. Download [chr1.fa.zip](https://bitbucket.org/shokhirev/maps/downloads/chr1.fa.zip), the genomic sequence for human chr1, and unzip it.
3. Run MAPS or [MAPS.jar](https://bitbucket.org/shokhirev/maps/downloads/MAPS.jar) with both the genomic sequence and aligned reads:

e.g. java -jar MAPS.jar -seq chr1.fa -consensus -library rf -mult 178 -p 4 chr1_first5m.bam

Note that the -library option is needed for correct strand orientation (depends on library type) and -mult is needed because we are only assembling a small proportion of reads which come from about 178 million fragments.
Also, the -p 4 option is provided to signify that 4 parallel threads should be used to speed up the assembly.

## Publication ##

MAPS is currently under review. Please come back soon for updated citation information.  
The paper describes the original [MAPS_original.jar](https://bitbucket.org/shokhirev/maps/downloads/MAPS_original.jar), which has since gone through several rounds of bug fixes and improvements.

## Troubleshooting ##

* Please ensure that you have the latest [java](https://java.com/en/download/) installed. 
* It is recommended to increase the java virtual machine memory size with the -Xmx16G option or use low-memory mode (-p 0 see below).
 
e.g. java -Xmx16G -jar MAPS.jar -seq chr1.fa -consensus -library rf -mult 178 -p 4 chr1_first5m.bam
or java -Xmx4G -jar MAPS.jar -seq chr1.fa -consensus -library rf -mult 178 -p 0 chr1_first5m.bam


* If you are getting a NULL_POINTER error, please ensure that you have specified the input file and all other files correctly.
* Other errors might be due to incorrect formats used as input. MAPS requires a sam or bam file for input, and also uses FASTA formated sequences with the -seq option.
* If you are getting index out of bound errors, this might indicate that your sequence file does not match your alignment file. Ensure that you used the same assembly for both.
* If MAPS freezes, this might indicate that you are running out of system memory. Please try running with -p 0 to turn using the slower memory-low mode.
* If MAPS is taking a long time to run, please decrease the diversity parameter (more than 0.7 might produce very large assemblies). 
* If you find that you are assembling transcripts and open reading frames on the opposite strand, please change the library type with the -library parameter
* If your favorite gene is not being assembled correctly, please play around with the diversity parameter, low expressed genes may be filtered out.
* Try loading the gff files into a genome browser to see the exon and junction assembled. This can help pinpoint why your gene was not assembled.
* Try running with the verbose -v option set to get more information about what is happening.
* If you are still having trouble, please email me (see below):

## Notes ##

* Provide a genomic sequence using the -seq parameter to generate a pgSNP file with detected SNPS from reads and provide the -consensus flag to correct the output sequences with consensus sequence data from the reads.
* The MAPS GTF files can be loaded in genome browsers to visualize the assembled transcript structures (For example UCSC genome browser or IGV)
* All filtered exon and junction information is output in gff format and can also be loaded into a genome browser for visualization (to check why a gene didn't get assembled)
* If you are only assembling one chromosome or a fraction of the reads, please adjust the mult parameter to the total number of reads in 10^6 aligned.
* minMAPQ can be specified to remove multi-mapping reads from the assembly. 
* adjust fmin and fmax to filter the size of the ORFs being reported.
* Adjust the diversity parameter carefully. Larger diversity can result in significantly larger transcriptomes and very long run times.
* Use -o to output the results with a specific prefix (by default the current date and time are used)

## Contact ##

If you have questions or bug reports please email Max: mshokhirev AT salk DOT edu after going through the troubleshooting section above. Please provide the commands and output from MAPS.

## License ##

 Copyright (c)  2015-2018   The Salk Institute for Biological Studies.
 All Rights Reserved

 Permission to copy, modify and distribute any part of this MAPS for educational, research and non-profit purposes, without fee, and without a written agreement is hereby granted, provided that the above copyright notice, this paragraph and the following three paragraphs appear in all copies.
 Those desiring to incorporate this MAPS into commercial products or use for commercial purposes should contact the Technology Transfer Office, The Salk Institute for Biological Studies, La Jolla, 10010 N Torrey Pines Rd., La Jolla, CA 92037, Ph: (858) 453-4100.
 IN NO EVENT SHALL THE SALK INSTITUTE FOR BIOLOGICAL STUDIES BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS MAPS, EVEN IF THE SALK INSTITUTE FOR BIOLOGICAL STUDIES HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 THE MAPS PROVIDED HEREIN IS ON AN "AS IS" BASIS, AND THE SALK INSTITUTE FOR BIOLOGICAL STUDIES HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.  THE SALK INSTITUTE FOR BIOLOGICAL STUDIES MAKES NO REPRESENTATIONS AND EXTENDS NO WARRANTIES OF ANY KIND, EITHER IMPLIED OR EXPRESS, 
 INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, OR THAT THE USE OF THE MAPS WILL NOT INFRINGE ANY PATENT, TRADEMARK OR OTHER RIGHTS.

